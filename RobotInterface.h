#pragma once
#include"Pose.h"
#include<iostream>
#include"RangeSensor.h"
#include"SonarSensor.h"
#include"LaserSensor.h"
/**
 * @fileRecord.h
 * @Author Mehmet �AKAR (152120181023@ogrenci.ogu.edu.tr)
 * @date Januvary, 2021
 * @brief Brief description of file.
 *
 * Detailed description of file.
 */
class RobotInterface
{
	Pose* position;
	int state;
public:
	/*!
	* Robot start to turn left
	*/
	//! turnLeft Function
	virtual void turnLeft() = 0;
	/*!
	* Robot start to turn right
	*/
	//! turnRight Function
	virtual void turnRight() = 0;
	/*!
	* speed define robot's forward speed
	*/
	//! Forward Function
	virtual void forward(float speed) = 0;
	//!Print Robot's details -> X Y Th
	virtual void print() = 0;
	/*!
	* speed define robot's backward speed
	*/
	//! Backward Function
	virtual void backward(float speed) = 0;
	/*!
	* Return Robot's position
	*/
	//! GetPose Function
	virtual Pose getPose() = 0;
	/*!
	* Take Pose and set it to robot's position
	*/
	//! setPose Function
	virtual void setPose(Pose* pos) = 0;
	/*!
	* Stop to Robot's turning right or left
	*/
	//!stopTurn Function
	virtual void stopTurn() = 0;
	/*!
	* Stop to Robot's moving forward or backward
	*/
	//! stopMove Function
	virtual void stopMove() = 0;
	/*!
	* This function update ranges from PioneerRobotAPI and set ranges into ranges[].
	*/
	//! updateSensor Function
	virtual void updateSensor() = 0;
};

