#pragma once
#include "PioneerRobotAPI.h"
#include"Pose.h"
#include "Aria/Aria.h"
#include<iostream>
#include<string>
#include"Record.h"
#include"Path.h"
#include"RobotOperator.h"
#include"RobotInterface.h"
#include "PioneerRobotInterface.h"
#include<vector>

using namespace std;
/**
 * @file RobotControl.h
 * @Author Sabri Sinan SA�LAM (152120181034@ogrenci.ogu.edu.tr)
 * @date 09.01.2021
 * @brief This class control robot's moves !
 *
 */
class RobotControl
{
	RobotInterface* robot_i/*=new PioneerRobotInterface()*/;
	Pose* position;
	PioneerRobotAPI* robotAPI;
	int state;
	Path path;
	Record record;
	RobotOperator robotoperator;
	bool access=false;
	vector<RangeSensor> sensors;
public:
	/*!
	* change state status true is mean connect
	*/
	//! setState Func
	void setState(bool);

	/*!
	* return connection state of robot
	*/
	//! getState Func 
	bool getState();

	/*!
	* Define robotAPI to main robot 
	*/
	//! Constructor Function
	RobotControl(PioneerRobotAPI*);

	//!Destructor Func
	~RobotControl();

	/*!
	* Robot start to turn left
	*/
	//! turnLeft Function
	void turnLeft();

	/*! 
	* Robot start to turn right 
	*/ 
	//! turnRight Function
	void turnRight();

	/*!
	* speed define robot's forward speed
	*/
	//! Forward Function
	void forward(float speed);

	//!Print Robot's details -> X Y Th
	void print();

	
	/*!
	* speed define robot's backward speed
	*/
	//! Backward Function
	void backward(float speed);

	/*!
	* Return Robot's position	
	*/
	//! GetPose Function
	Pose getPose();

	/*!
	* Take Pose and set it to robot's position
	*/
	//! setPose Function
	void setPose(Pose*);

	/*!
	* Stop to Robot's turning right or left
	*/
	//!stopTurn Function
	void stopTurn();

	/*!
	* Stop to Robot's moving forward or backward
	*/
	//! stopMove Function
	void stopMove();
	
	/*!
	* The location of the robot is added to the path.  
	*/
	//! addToPath Function
	bool addToPath();

	/*!
	* Clear all path
	*/
	//! clearPath Function
	bool clearPath();

	/*!
	* All locations loaded in the Path object are printing
	*/
	//! recordPath Func
	bool recordPath();

	/*!
	* Open access if password is true
	*/
	//! openAccess Function
	bool openAccess(int);

	bool closeAccess(int);
};

