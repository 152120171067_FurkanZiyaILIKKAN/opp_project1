#include "RobotOperator.h"
//! Setting name, surname and accessCode
RobotOperator::RobotOperator(string name, string surname, int parola)
{ 
    this->name = name;
    this->surname = surname;
    accessCode = encryptCode(parola);
}
//! With the Encryption class, parola is encrypting here
int RobotOperator::encryptCode(int a)
{
    Encryption c;
    accessCode= c.encryption(a);
    return accessCode;
}
//! With the Encryption class, parola is decrypt�ng here
int RobotOperator::decryptCode(int a)
{ 
    Encryption c;
    return c.decryption(a);
}
//! Checking the parola for its size and also its correctness

bool RobotOperator::checkAccessCode(int a)
{
    if (a>9999)
    {
        cout << "Parola is so long!!"<<endl;
        return false;
    }
    else if (a < 1000) {
        cout << "Parola is so short!!" << endl;
        return false;
    }
   
    if ( decryptCode(accessCode)== a)
    {
        accessState = true;
        return true;
    }
    accessState = false;
    return false;
}
//! Print function can  print the name , surname and accessState
void RobotOperator::print()
{
    if (accessState==true)
    {
        cout << "Name: " << name << endl << "Surname: " << surname << endl;
        cout << "Access allowed"<<endl;       
    } 
    else {
        cout << "Access is not allowed!!" << endl;
    } 
}
