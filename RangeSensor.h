#pragma once
#include "PioneerRobotAPI.h"
/**
 * @file Pose.h
 * @Author Furkan Ziya ILIKKAN (152120171067@ogrenci.ogu.edu.tr)
 * @date 23.01.2021
 * @brief This file is about Pose.
 *
 * Detailed description of file.
 */
class RangeSensor
{
public:
	/*!
	* This function return float variable from ranges at index.
	*/
	//! getRange Function
	virtual float getRange(int)=0;
	/*!
	* This function get maximum range from ranges[].
	*/
	//! getMax Function
	virtual float getMax(int&)=0;
	/*!
	* This function get minimum range from ranges[].
	*/
	//! getMin Function
	virtual float getMin(int&)=0;
	/*!
	* This function update ranges from PioneerRobotAPI and set ranges into ranges[].
	*/
	//! updateSensor Function
	virtual void updateSensor(float[])=0;
	/*!
	* This function return variable at ranges[int].
	*/
	//! operator [] Function
	virtual float operator[](int)=0;
	/*!
	* This function return angle at ranges[int].
	*/
	//! getAngle Function
	virtual float getAngle(int)=0;
	/*!
	* This function return closest range at between given ranges.
	*/
	//! ggetClosestRange Function
	virtual float getClosestRange(float, float, float&)=0;
private:
	float ranges[724];
	PioneerRobotAPI* robotAPI;
};