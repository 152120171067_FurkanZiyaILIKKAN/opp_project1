#include "Record.h"

//! Opening the file with file name which takes from the user.
//!if it can open , then return true else return false

bool Record::openFile()
{
	file.open(fileName);
	if (!file.is_open())
	{
		cout << "File can not open at the moment!!" << endl;
		return false;
	}
	cout << "File has opened!!" << endl;
	return true;
}
//! if file is open, then file will close here
//! if file can close , then return true else return false
bool Record::closeFile()
{
	if (file.is_open())
	{
		file.close();
		cout << "File has closed!!" << endl;
		return true;
	}
	
	cout << "File can not close or there is not any file according to close!!" << endl;
	return false;
}
//! We take the paramtre of STRING of the file from the user

void Record::setFileName(string name)
{
	fileName = name;
}
//! Reading the line from the file, if the file open of couse
string Record::readLine()
{
	if (file.is_open())
	{
		string line;
		
			getline(file, line);
			cout << line << endl;
			return line;
		
	}
	else
		cout << "File has been closed or never has opened. So can not read the line!!" << endl; exit(1);

	//cout << "Program can not read line from the file!!"<<endl;
	
}
//! Write sth(STRING) to the file's line, if the file is open of course.
bool Record::writeLine(string str)
{
	ofstream file1(fileName, ios::app);
	if (file.is_open())
	{
	
		file1 << str<<endl;
		cout << str << " is written inside the file!!" << endl;
		return true;
	}
	cout << "Nothing has been written to the file!!" << endl;
	return false;
}

//Record& Record::operator<<()
//{
//	// TODO: insert return statement here
//
//
//}
//
//Record& Record::operator>>()
//{
//	// TODO: insert return statement here
//}
