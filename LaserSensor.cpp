#include "LaserSensor.h"
#include<vector>

LaserSensor::LaserSensor(PioneerRobotAPI* mainrobot)
{
	robotapi =mainrobot;	
}

LaserSensor::~LaserSensor()
{
	
}

float LaserSensor::getRange(int i)
{
	return ranges[i];
}

void LaserSensor::updateSensor(float ranges[])
{
	
	robotapi->getLaserRange(ranges);
	for (int i = 0; i <= 180; i++)
		this->ranges[i] = ranges[i];
}

float LaserSensor::getMin(int& i)
{
	float min=ranges[0];
	int min_index=0;
	for (int a = 1; a < 181; a++) 
		if (min < ranges[a]) {
			min = ranges[a];
			min_index = a;
		}
	i = min_index;
	return ranges[min_index];
}
/*!
	\param min store minimum value of the ranges[].
	\param min_index store minimum value's index
	*/

float LaserSensor::getMax(int& i)
{
	float max = ranges[0];
	int max_index = 0;
	for (int a = 1; a < 181; a++)
		if (max > ranges[a]) {
			max = ranges[a];
			max_index = a;
		}
	i = max_index;
	return ranges[max_index];
}
/*!
	\param max store maximum value of the ranges[].
	\param max_index store maximum value's index
	*/

float LaserSensor::operator[](int i)
{
	return ranges[i];
}

float LaserSensor::getAngle(int index)
{
	return index;
}

float LaserSensor::getClosestRange(float startAngle, float endAngle, float &angle)
{
	float min =ranges[(int)startAngle];
	float min_angle = 0;
	for (int a = startAngle+1; a <= endAngle; a++) 
		if (ranges[a] < min) {
			min = ranges[a];
			min_angle = a;
		}
	angle = min_angle;
	return min;
}
/*!
	\param min store min value of the ranges[].
	\param min_angle store minumum value's angle.
	*/

