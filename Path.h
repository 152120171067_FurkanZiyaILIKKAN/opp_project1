#pragma once
#include "PioneerRobotAPI.h"
#include "Aria/Aria.h"
#include<iostream>
#include"Node.cpp"
using namespace std;
/**
 * @file Path.h
 * @Author Sabri Sinan SA�LAM (152120181034@ogrenci.ogu.edu.tr)
 * @date 09.01.2021
 * @brief This class keep robot's path !
 *
 */
class Path
{

	Node* tail;
	Node* head;
	
public:
	int number;
	Path();
	/*!
	* define NULL to head and tail
	*/
	//! Constructor Func
	
	~Path();
	//!Destructor Func
	
	void addPose(Pose pose);
	/*!
	* Adding pose to path
	*/
	//! addPose Func

	void print();
	/*!
	* Print all pose in path 
	*/
	//! print Func
	
	Node operator [](int i);
	/*!
	* Return node in that operator --> [ ]
	*/
	//! [ ] operator
	
	Pose getPose(int index);
	/*!
	* Return position of that index in path
	*/
	//! getPose Func
	
	bool removePose(int index);
	/*!
	* Remove pose from path in that index 
	*/
	//! removePose Func
	
	bool insertPos(int index, Pose pose);
	/*!
	* Adding pose to that index
	*/
	//! insertPos
	
	void operator << (int i);
	/*!
	* Print all path ! right of operator is random int
	*/
	//! << operator
	
	Path operator >>(Pose pose);
	/*!
	* Add pose to path 
	*/
	//! >> operator

};

