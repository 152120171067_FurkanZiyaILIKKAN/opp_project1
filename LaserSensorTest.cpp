#include<iostream>
#include"LaserSensor.h"
#include "PioneerRobotAPI.h"
using namespace std;

#if 1
int main() {
	PioneerRobotAPI* robot = new PioneerRobotAPI;
	robot->connect();
	LaserSensor test1(robot);
	int min, max;
	float range1 = 30;
	float range2 = 150;
	float closest_range_angle;
	float a[181];
	//updateSensor test
	test1.updateSensor(a);
	//operator[] test
	cout << "Ranges: [ ";
	for (int i = 0; i <= 180; i++)
		cout << test1[i] << " ,";
	cout << "]" << endl;
	//getRange test
	cout << "Range at 0 is " << test1.getRange(0) << endl;
	cout << "Range at 180 is " << test1.getRange(180) << endl;
	//getMin and getMax test
	test1.getMin(min);
	test1.getMax(max);
	cout << "Minumum's Angle = " << min << " Minumum's Value = " << test1.getMin(min) << endl;
	cout << "Maximum's Angle = " << max << " Maximum's Value = " << test1.getMax(max) << endl;
	//getClosestRange test
	test1.getClosestRange(range1, range2, closest_range_angle);
	cout << "Closest Range between" << range1 << " and " << range2 << " is " << test1.getClosestRange(range1, range2, closest_range_angle) << " and Angle is " << closest_range_angle << endl;
	return 0;
	system("PAUSE");
}
#endif