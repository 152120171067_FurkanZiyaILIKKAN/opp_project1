#include "Pose.h"
#include<cmath>

Pose::Pose()
{
    this->x = 0;
    this->y = 0;
    this->th = 0;
}

float Pose::getX()
{
    return x;
}

void Pose::setX(float x)
{
    this->x = x;
}
float Pose::getY()
{
    return y;
}

void Pose::setY(float y)
{
    this->y = y;
}
float Pose::getTh()
{
    return th;
}

void Pose::setTh(float th)
{
    this->th = th;
}

bool Pose::operator==(const Pose& other)
{
    if (x == other.x && y == other.y && th == other.th)
        return true;
    else
        return false;
}

Pose Pose::operator+(const Pose& other)
{
    Pose a;
    a.x = this->x + other.x;
    a.y = this->y + other.y;
    a.th = this->th + other.th;
    return a;
}
/*!
    \param a is a Pose object. We need to return this object without changing pose and others variables.
    */

Pose& Pose::operator+=(const Pose& other)
{

    x += other.x;
    y += other.y;
    th += other.th;
    return *this;
}

Pose Pose::operator-(const Pose& other)
{
    Pose a;
    a.x = other.x -this->x;
    a.y = other.y - this->y;
    a.th = other.th - this->th;
    return a;
}
/*!
    \param a is a Pose object. We need to return this object without changing pose and others variables.
    */

Pose& Pose::operator-=(const Pose& other)
{
    x -= other.x;
    y -= other.y;
    th -= other.th;
    return *this;
}

bool Pose::operator<(const Pose& other)
{
    if (x < other.x && y < other.y && th < other.th)
        return true;
    else
        return false;
}

void Pose::getPose(float& x, float& y, float&th)
{
    th = this->th;
    x = this->x;
    y = this->y;
}

void Pose::setPose(float x, float y, float th)
{
    this->x = x;
    this->y = y;
    this->th = th;
}

float Pose::findDistanceTo(Pose pos)
{
    return sqrt(pow((pos.x-x),2)+ pow((pos.y - y), 2));
}

float Pose::findAngleTo(Pose pos)
{
    float angle = atan2(abs(pos.y - y), abs(pos.x - x));
    angle *= 180 / M_PI;
    return angle;
}
/*!
    \angle a is float. This param holds the angle in radians then its convert to degree.
*/
