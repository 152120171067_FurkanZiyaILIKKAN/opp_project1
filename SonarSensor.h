#pragma once
/**
 * @file SonarSensor.h
 * @Author Mehmet �AKAR (152120181023@ogrenci.ogu.edu.tr)
 * @date Januvary, 2021
 * @brief Brief description of file.
 *
 * Detailed description of file.
 */
#include<iostream>
#include<string>
#include<fstream>
#include"PioneerRobotAPI.h"
#include"RangeSensor.h"
using namespace std;
class SonarSensor:public RangeSensor
{
private:
	float ranges[16];
	PioneerRobotAPI* robotAPI=new PioneerRobotAPI;

public:
	SonarSensor(PioneerRobotAPI*);
	float getRange(int);
	float getMax(int&);
	float getMin(int&);
	void updateSensor(float[]);
	float operator[](int);
	float getAngle(int);
	float getClosestRange(float, float, float&);

};

