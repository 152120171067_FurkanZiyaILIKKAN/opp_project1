#pragma once
#include"PioneerRobotAPI.h"
#include "RobotInterface.h"
#include"RobotControl.h"
#include<iostream>
/**
 * @file RobotControl.h
 * @Author Sabri Sinan SA�LAM (152120181034@ogrenci.ogu.edu.tr)
 * @date 23.01.2021
 * @brief This class control robot's moves !
 *
 */
using namespace std;
class PioneerRobotInterface :public RobotInterface
{
	PioneerRobotAPI* robotAPI;
	RangeSensor* sonarsensor ;
	RangeSensor* lasersensor ;

public:
	PioneerRobotInterface(PioneerRobotAPI*);
	/*!
	* Robot start to turn left
	*/
	//! turnLeft Function
	virtual void turnLeft();
	/*!
	* Robot start to turn right
	*/
	//! turnRight Function
	virtual void turnRight();
	/*!
	* speed define robot's forward speed
	*/
	//! Forward Function
	virtual void forward(float speed);
	//!Print Robot's details -> X Y Th
	virtual void print();
	/*!
	* speed define robot's backward speed
	*/
	//! Backward Function
	virtual void backward(float speed);
	/*!
	* Return Robot's position
	*/
	//! GetPose Function
	virtual Pose getPose();
	/*!
	* Take Pose and set it to robot's position
	*/
	//! setPose Function
	virtual void setPose(Pose* pose);
	/*!
	* Stop to Robot's turning right or left
	*/
	//!stopTurn Function
	virtual void stopTurn();
	/*!
	* Stop to Robot's moving forward or backward
	*/
	//! stopMove Function
	virtual void stopMove();
	/*!
	* This function update ranges from PioneerRobotAPI and set ranges into ranges[].
	*/
	//! updateSensor Function
	virtual void updateSensor();

};

