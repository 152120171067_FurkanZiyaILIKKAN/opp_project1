#pragma once
/**
 * @fileRecord.h
 * @Author Mehmet �AKAR (152120181023@ogrenci.ogu.edu.tr)
 * @date Januvary, 2021
 * @brief Brief description of file.
 *
 * Detailed description of file.
 */

#include<iostream>
#include<string>
#include<fstream>
using namespace std;
class Record
{
private:
	string fileName;
	fstream file;
public:
	bool openFile();
	bool closeFile();
	void setFileName(string name);
	string readLine();
	bool writeLine(string str);
	
	//Record& operator<<();
	//Record& operator>>();
};

