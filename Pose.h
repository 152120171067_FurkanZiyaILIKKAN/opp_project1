#pragma once
/**
 * @file Pose.h
 * @Author Furkan Ziya ILIKKAN (152120171067@ogrenci.ogu.edu.tr)
 * @date 09.01.2021
 * @brief This file is about Pose.
 *
 * Detailed description of file.
 */
#include<iostream>
#include"PioneerRobotAPI.h"

using namespace std;
class Pose
{
private:
	float x, y, th;
public:
	Pose();
	float getX();
	/*!
	* Return value of x pose has.
	*/
	//! getX Function
	void setX(float);
	/*!
	* float variable set to x
	*/
	//! setX Function
	float getY();
	/*!
	* Return value of y pose has.
	*/
	//! getY Function
	void setY(float);
	/*!
	* float variable set to y
	*/
	//! setY Function
	float getTh();
	/*!
	* Return value of th pose has.
	*/
	//! getTh Function
	void setTh(float);
	/*!
	* float variable set to th
	*/
	//! setTh Function
	bool operator ==(const Pose& other);
	/*!
	* This function equalize object's variables.
	*/
	//! operator == Function
	Pose operator +(const Pose& other);
	/*!
	* This function add object's variables.
	*/
	//! operator + Function
	Pose& operator +=(const Pose& other);
	/*!
	* This function add other's objesct's variables to this object's variables.
	*/
	//! operator += Function
	Pose operator -(const Pose& other);
	/*!
	* This function subtract variables
	*/
	//! operator - Function
	Pose& operator -=(const Pose& other);
	/*!
	* This function subtract other's variables from this object's variables
	*/
	//! operator -= Function
	bool operator <(const Pose& other);
	/*!
	* This function checks  other object's veriables is less than that object's variables
	*/
	//! operator < Function
	void getPose(float&, float&, float&);
	/*!
	* Get x y and th variables pose has.
	*/
	//! getPose Function
	void setPose(float, float, float);
	/*!
	* Set x y and th variables pose has.
	*/
	//! setPose Function
	float findDistanceTo(Pose);
	/*!
	* This function find distance between Poses
	*/
	//! findDistanceTo Function
	float findAngleTo(Pose);
	/*!
	* This function find angle between Poses
	*/
	//! findAngleTo Function
};

