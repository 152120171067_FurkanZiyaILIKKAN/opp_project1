#include "RobotControl.h"



void RobotControl::setState(bool state)
{
	this->state = state;
}

bool RobotControl::getState()
{
	return state;
}

RobotControl::RobotControl(PioneerRobotAPI* robot_)
{
	robotAPI = robot_;	
	robot_i = new PioneerRobotInterface(robotAPI);
	robotoperator.set_operator("Mali", "Zeybek", 4343);
}

RobotControl::~RobotControl()
{
	delete position;
	delete robotAPI;
}

void RobotControl::turnLeft()
{
	robot_i->turnLeft();
}

void RobotControl::turnRight()
{
	robot_i->turnRight();
}

void RobotControl::forward(float speed)
{
	robot_i->forward(speed);
}

void RobotControl::print()
{
	robot_i->print();
}

void RobotControl::backward(float speed)
{
	robot_i->backward(speed);
}

Pose RobotControl::getPose()
{
	return robot_i->getPose();
}

void RobotControl::setPose(Pose* pose)
{
	robot_i->setPose(pose);
}

void RobotControl::stopTurn()
{
	robot_i->stopTurn();
}

void RobotControl::stopMove()
{
	robot_i->stopMove();
}

bool RobotControl::addToPath()
{
	path.addPose(getPose());	
	return true;	
}

bool RobotControl::recordPath()
{
	record.setFileName("all_path");
	if (!record.openFile())
	{
		return false;
	}	
	for (int i = 0; i < path.number; i++)
	{
		float x, y, th;
		getPose().getPose(x, y, th);
		string pose_string = "";
		pose_string += "Robot's pose: x= ";
		pose_string += to_string(x);
		pose_string += " | y= ";
		pose_string += to_string(y);
		pose_string += " | th= ";
		pose_string += to_string(th);
		pose_string += "\n";
		record << pose_string;
	}
	
	return true;
}

bool RobotControl::openAccess(int pass)
{
	if (robotoperator.checkAccessCode(pass))
	{
		access = true;
		return true;
	}
	else
	{
		access = true;
		return false;
	}
}

bool RobotControl::closeAccess(int pass)
{
	if (robotoperator.checkAccessCode(pass))
	{
		access = false;
		return true;
	}
	else
	{
		access = true;
		return false;
	}
}

bool RobotControl::clearPath()
{	
	while (path[0].next)
	{
		path.removePose(0);
	}
	path.removePose(0);
	return true;
}