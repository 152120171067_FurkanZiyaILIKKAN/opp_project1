#include"Pose.h"
#include<iostream>

using namespace std;


#if 0
int main(){
	Pose test1;
	float a, b, c;
	//setPose ve getPose fonksiyonlar� testi
	test1.setPose(5, 16, 20);
	test1.getPose(a,b,c);
	cout << a << " " << b << " " << c << " " << endl;
	if (test1.getX() == a && test1.getY() == b && test1.getTh()==c)
		cout << "getX, getY & getTh fonksiyonlari dogru calisiyor." << endl;
	test1.setX(1);
	test1.setY(1);
	test1.setTh(1);
	if (test1.getX() == 1 && test1.getY() == 1 && test1.getTh()==1)
		cout << "setX, setY & setTh fonksiyonlari dogru calisiyor." << endl;
	//== operatoru testi
	Pose test2, test3;
	test2.setPose(5.5, 3.2, 9.9);
	test3.setPose(1, 1, 1);
	if (test1 == test2) 
		cout << "Test1 esittir Test2" << endl;
	
	else 
		cout << "Test1 esit degildir Test2" << endl;
	if (test1 == test3)
		cout << "Test1 esittir Test3" << endl;

	else
		cout << "Test1 esit degildir Test3" << endl;

	//+ operatoru testi
	cout <<"Test2 + Test 3 = "<< (test2 + test3).getX() <<" "<<(test2 + test3).getY() <<" "<< (test2 + test3).getTh() << endl;
	//- operatoru testi
	cout << "Test2 - Test 3 = " << (test2 - test3).getX() << " " << (test2 - test3).getY() << " " << (test2 - test3).getTh() << endl;
	//+= operatoru testi
	test2 += test3;
	cout << "Test2 += Test 3 -----> " << test2.getX() << " " << test2 .getY() << " " << test2.getTh() << endl;
	//-= operatoru testi
	test2 -= test3;
	cout << "Test2 -= Test 3 -----> " << test2.getX() << " " << test2.getY() << " " << test2.getTh() << endl;
	//findDistanceTo fonksiyonu test1
	Pose test4, test5;
	test4.setPose(2, 0, 0);
	test5.setPose(5, 0, 0);
	if (test4.findDistanceTo(test5) == 3)
		cout << "findDistanceTo fonksiyonu do�ru";
	else {
		cout << "findDistanceTo fonksiyonu yanlis";
	}
	cout<<endl;
	//findDistanceTo fonksiyonu test2
	test4.setPose(1, 3, 0);
	test5.setPose(5, 9, 0);
	if (test4.findDistanceTo(test5) == 7.2111)
		cout << "findDistanceTo fonksiyonu do�ru";
	else {
		cout << "findDistanceTo fonksiyonu yanlis";
	}
	//findAngleTo fonksiyonu testi
	test4.setPose(3, 4, 0);
	test5.setPose(10, 12, 0);
	cout << test4.findAngleTo(test5) << endl;
	
}
#endif