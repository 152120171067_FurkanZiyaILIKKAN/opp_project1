#pragma once
/**
 * @file RobotOperatorh
 * @Author Mehmet �AKAR (152120181023@ogrenci.ogu.edu.tr)
 * @date Januvary, 2021
 * @brief Brief description of file.
 *
 * Detailed description of file.
 */
#include<iostream>
#include<string>
#include<fstream>
#include"Encryption.h"
using namespace std;
class RobotOperator
{
private:
	string name;
	string surname;
	unsigned int accessCode;
	bool accessState;

public:
	RobotOperator(string,string,int);
	int encryptCode(int);
	int decryptCode(int);
	bool checkAccessCode(int);
	void print();
};

