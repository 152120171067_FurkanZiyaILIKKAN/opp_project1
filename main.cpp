#include"RobotControl.h"
#include"Pose.h"
#include"Path.h"
#include<iostream>
#include"PioneerRobotAPI.h"
#include "SonarSensor.h"
#include"LaserSensor.h"
#include"RobotOperator.h"
#include"RangeSensor.h"
#if 1
PioneerRobotAPI* robotmain = new PioneerRobotAPI;
RobotControl robot(robotmain);
RobotOperator user1;
Pose pose_random;
Path path;
RangeSensor* sonar;
RangeSensor* laser;
void Main_menu();
void Connection_menu();
void Motion_menu();
void Sensor_menu();
void Path_menu();
float x = 1, y = 1, th = 1;
float Speed = 100;
float sonar_[16], laser_[724];

int main()
{
	user1.set_operator("Dave", "July ", 1234);
	Main_menu();
	return 0;
}

void Main_menu()
{	
	system("cls");
	int main_answer = -2;
	cout << "\n--------------MAIN MENU--------------" << endl << endl;
	cout << "\t1. Connection" << endl;
	cout << "\t2. Motion" << endl;
	cout << "\t3. Sensor" << endl;
	cout << "\t4. Path" << endl;
	cout << "\t5. Quit" << endl;
	cout << "\tChoose one : ";
	cin >> main_answer;
	cout << "--------------------------------" << endl;
	if (main_answer == 1)
	{
		Connection_menu();
	}
	else if (main_answer == 2 && robot.getState())
	{
		Motion_menu();
	}
	else if (main_answer == 3 && robot.getState())
	{
		Sensor_menu();
	}
	else if (main_answer == 4 && robot.getState())
	{
		Path_menu();
	}
	else if (main_answer == 5)
	{
		return;
	}
	else if (main_answer > 0 && main_answer < 5)
	{
		cout << "\n\tYou dont have access enter access code!!\n";
	}
	else
	{
		cout << "\n\n!!!!! You entered wrong number (try again) !!!!!\n\n";
	}
	Main_menu();
}

void Connection_menu() 
{
	int connection_answer = -1;
	cout << "--------------Connection MENU--------------" << endl << endl;
	cout << "\t1. Connect" << endl;
	cout << "\t2. Disconnect" << endl;
	cout << "\t3. Open Access" << endl;
	cout << "\t4. Close Access" << endl;
	cout << "\t5. Back" << endl;
	cout << "\t   Choose one: ";
	cin >> connection_answer;
	cout << "--------------------------------" << endl;
	if (connection_answer == 1)
	{
		if (!robot.getState())
		{
			cout << "\n\tYou dont have access enter access code!!\n";
		}
		else
		{
			robotmain->connect();
			robotmain->setPose(0, 0, 0);
			robot.setState(true);
			cout << "<Connected>" << endl;
		}
	}
	else if (connection_answer == 2)
	{
		if (!robot.getState())
		{
			cout << "\n\tYou dont have access enter access code!!\n";
		}
		else
		{
			robotmain->disconnect();
			robot.setState(false);
			cout << "<Disconnected>" << endl;
		}
	}
	else if (connection_answer == 3)
	{
		unsigned int pass;
		cout << "\nEnter password: ";
		cin >> pass;
		if (user1.checkAccessCode(pass))
		{
			robot.setState(true);
			user1.print();
		}
		else
		{
			cout << "ERROR!! You entered false password!!\n";
		}
		cout << endl;		
	}
	else if (connection_answer == 4)
	{
		if (!robot.getState())
		{
			cout << "\n\tYou dont have access enter access code!!\n";
		}
		else
		{
			user1.print();
			unsigned int pass;
			cout << "\nEnter password: ";
			cin >> pass;
			if (user1.checkAccessCode(pass))
			{
				robot.setState(false);
				cout << "\nAccess is closed !!\n";
			}
			else
			{
				cout << "ERROR!! You entered false password!!\n";
			}
			cout << endl;
		}
	}
	else if (connection_answer == 5)
	{
		return;
	}
	else
	{
		cout << "\n\n!!!!! You entered wrong number (try again) !!!!!\n\n";
	}
	Connection_menu();

}

void Motion_menu()
{
	int motion_answer = -1;
	cout << "--------------Motion MENU--------------" << endl << endl;
	cout << "\t1. Forward" << endl;
	cout << "\t2. Backward" << endl;
	cout << "\t3. Turn Right" << endl;
	cout << "\t4. Turn Left" << endl;
	cout << "\t5. Stop Turn" << endl;
	cout << "\t6. Stop Move" << endl;
	cout << "\t7. Set Pose" << endl;
	cout << "\t8. Print Pose" << endl;
	cout << "\t9. Quit" << endl << endl;
	cout << "\t   Choose one : ";
	cin >> motion_answer;
	cout << "--------------------------------" << endl;
	if (motion_answer == 1)
	{
		float speed;
		cout << "Enter speed:" << endl;
		cin >> speed;
		robot.forward(speed);

	}
	else if (motion_answer == 2)
	{
		float speed;
		cout << "Enter speed:" << endl;
		cin >> speed;
		robot.backward(speed);
	}
	else if (motion_answer == 3)
	{
		robot.turnRight();
	}
	else if (motion_answer == 4)
	{
		robot.turnLeft();
	}
	else if (motion_answer == 5)
	{
		robot.stopTurn();
	}
	else if (motion_answer == 6)
	{
		robot.stopMove();

	}
	else if (motion_answer == 7)
	{
		float x, y, th;
		cout << "Enter robot's new position(x , y , th ):";
		cin >> x >> y >> th;
		Pose* new_pose = new Pose();
		//new_pose->setPose(x, y, th);
		robotmain->setPose(x, y, th);
		cout << endl << "Robot is in new position" << endl;
	}
	else if (motion_answer == 8)
	{
		cout << "\nRobot's position:";
		robot.print();
		cout << endl;
	}
	else if (motion_answer == 9)
	{
		return;
	}
	else
	{
		cout << "\n\n!!!!! You entered wrong number (try again) !!!!!\n\n";
	}
	Motion_menu();
}

void Sensor_menu()
{
	sonar = new SonarSensor(robotmain);
	laser = new LaserSensor(robotmain);
	sonar->updateSensor(sonar_);
	laser->updateSensor(laser_);
	int sensor_answer = -2;
	cout << "--------------Sensor MENU--------------" << endl << endl;
	cout << "\t1. Get max of sonar sensor" << endl;
	cout << "\t2. Get min of sonar sensor" << endl;
	cout << "\t3. Get max of laser sensor" << endl;
	cout << "\t4. Get min of laser sensor" << endl;
	cout << "\t5. Get closest range of laser sensor" << endl;
	cout << "\t6. Quit" << endl;
	cout << "\t Choose one : ";
	cin >> sensor_answer;
	cout << "--------------------------------" << endl;
	if (sensor_answer == 1)
	{
		float maxOfSonar;
		int maxOfSonarangle;
		maxOfSonar = sonar->getMax(maxOfSonarangle);
		cout << "\nMax range is :" << maxOfSonar << endl;
	}
	else if (sensor_answer == 2)
	{
		float minOfSonar;
		int minOfSonarangle;
		minOfSonar = sonar->getMin(minOfSonarangle);
		cout << "\nMin range is :" << minOfSonar << endl;
	}
	else if (sensor_answer == 3)
	{
		float maxOfLaser;
		int maxOfLaserangle;
		maxOfLaser = laser->getMax(maxOfLaserangle);
		cout << "\nMax range is :" << maxOfLaser << endl;
	}
	else if (sensor_answer == 4)
	{
		float minOfLaser;
		int minOfLaserangle;
		minOfLaser = laser->getMin(minOfLaserangle);
		cout << "\nMin range is :" << minOfLaser << endl;
	}
	else if (sensor_answer == 5)
	{
		float min_angle = 30, max_angle = 90;//default 
		float range_;
		cout << "Enter min angle and max angle (find between them) to find closest range:";
		cin >> min_angle >> max_angle;
		cout << "\nThe closest range : " << laser->getClosestRange(30, 90, range_);
		cout << "\nThe closest range's angle : " << range_ << endl << endl;

	}
	else if (sensor_answer == 6)
	{
		return;
	}
	else
	{
		cout << "\n\n!!!!! You entered wrong number (try again) !!!!!\n\n";
	}	
	Sensor_menu();
}

void Path_menu()
{	
	int path_answer = -2;
	cout << "--------------Path MENU--------------" << endl << endl;
	cout << "\t1. Add Pose" << endl;
	cout << "\t2. Get Pose" << endl;
	cout << "\t3. Remove Pose" << endl;
	cout << "\t4. Insert Pose" << endl;
	cout << "\t5. Print" << endl;
	cout << "\t6. Quit" << endl;
	cout << "\tChoose one : ";
	cin >> path_answer;
	cout << "--------------------------------" << endl;
	if (path_answer == 1)
	{
		float x1, y1, th1;
		Pose adding_pose;
		cout << "\nEnter x - y - th :";
		cin >> x1 >> y1 >> th1;
		adding_pose.setPose(x1, y1, th1);
		path.addPose(adding_pose);
		cout << endl;
	}
	else if (path_answer == 2)
	{
		int index_ = -1;
		cout << "Enter index of you want to see pose" << endl;
		cin >> index_;
		Pose ans_pose = path.getPose(index_);
		cout << "Pose is:";
		cout << "x:| " << ans_pose.getX() << " |";
		cout << "y:| " << ans_pose.getY() << " |";
		cout << "th:| " << ans_pose.getTh() << " |\n";
	}
	else if (path_answer == 3)
	{
		int index_ = -1;
		cout << "Enter index of you want to remove pose" << endl;
		cin >> index_;
		if (path.removePose(index_))
		{
			cout << "Succesfull remove !\n";
		}
		else
		{
			cout << "Unsuccesdull remove !!\n";
		}
	}
	else if (path_answer == 4)
	{
		float x1, y1, th1;
		Pose adding_pose;
		cout << "\nEnter x - y - th :";
		cin >> x1 >> y1 >> th1;
		adding_pose.setPose(x1, y1, th1);
		int index_ = -1;
		cout << "\nEnter index:";
		cin >> index_;
		path.insertPos(index_, adding_pose);
	}
	else if (path_answer == 5)
	{
		path.print();
		//path<<1;
	}
	else if (path_answer == 6)
	{
		return;
	}
	else
	{
		cout << "\n\n!!!!! You entered wrong number (try again) !!!!!\n\n";
	}	
	Path_menu();
}

#endif