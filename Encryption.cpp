#include "Encryption.h"
//! eplace each digit with the result of adding 7 o the digit 
//! and getting the remainder after dividing the new value by 1 0. 
//! Then swap the first digit with the third, and swap 
//! the second digit with the fourth
int Encryption::encryption(int parola)
{
	int a = parola / 1000;
	int b = (parola % 1000) / 100;
	int c = (parola % 100) / 10;
	int d = parola % 10;
	a = (a + 7) % 10;
	b = (b + 7) % 10;
	c = (c + 7) % 10;
	d = (d + 7) % 10;
	return (c * 1000 + d * 100 + a * 10 + b); 
}
//!Write a separate application that inputs an encrypted
//!  four� digit integer and decrypts it (by reversing the encryption scheme) to form 
//! the original number. 
int Encryption::decryption(int parola)
{

	int a = parola / 1000;
	int b = (parola % 1000) / 100;
	int c = (parola % 100) / 10;
	int d = parola % 10;
	a = (a + 3) % 10;
	b = (b + 3) % 10;
	c = (c + 3) % 10;
	d = (d + 3) % 10;
	return (c * 1000 + d * 100 + a * 10 + b);
}
