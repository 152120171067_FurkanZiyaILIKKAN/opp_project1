#include "PioneerRobotInterface.h"



PioneerRobotInterface::PioneerRobotInterface(PioneerRobotAPI* id)
{
	robotAPI= new PioneerRobotAPI;
	robotAPI = id;
	sonarsensor = new SonarSensor(robotAPI);
	lasersensor = new LaserSensor(robotAPI);
}

void PioneerRobotInterface::turnLeft()
{
	
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
	cout << "<Robot is turning to left>" << endl;
	
}
void PioneerRobotInterface::turnRight()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
	cout << "<Robot is turning to right>" << endl;
}

void PioneerRobotInterface::forward(float speed)
{
	robotAPI->moveRobot(speed);
	cout << "<Robot is going forward>" << endl;
	Sleep(1000);
}

void PioneerRobotInterface::print()
{
	cout << "Robot's position x: | " << robotAPI->getX() << " |" << endl;
	cout << "Robot's position y: | " << robotAPI->getY() << " |" << endl;
	cout << "Robot's degree  th: | " << robotAPI->getTh() << " |" << endl;
	cout << endl;
}

void PioneerRobotInterface::backward(float speed)
{
	robotAPI->moveRobot(-speed);
	Sleep(1000);
	cout << "<Robot is going backward>" << endl;
}

Pose PioneerRobotInterface::getPose()
{
	Pose* position=new Pose();
	position->setPose(robotAPI->getX(), robotAPI->getY(), robotAPI->getTh());
	return *position;
}

void PioneerRobotInterface::setPose(Pose* pose)
{
	robotAPI->setPose(pose->getX(), pose->getY(), pose->getTh());
}

void PioneerRobotInterface::stopTurn()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::forward);
	cout << "<Robot stops turn>" << endl;

}

void PioneerRobotInterface::stopMove()
{
	robotAPI->moveRobot(0);
	cout << "<Robot stops move>" << endl;
}

void PioneerRobotInterface::updateSensor()
{
	float laser_[724], sonar_[16];
	sonarsensor->updateSensor(sonar_);
	lasersensor->updateSensor(laser_);
}
