#pragma once
#include<iostream>
#include"PioneerRobotAPI.h"
#include"RangeSensor.h"
using namespace std;

class LaserSensor:public RangeSensor
{
private:
	float ranges[181];
	PioneerRobotAPI *robotapi = new PioneerRobotAPI;
public:
	LaserSensor(PioneerRobotAPI*);
	/*!
	* Constructer function
	*/
	//! LaserSensor Function
	~LaserSensor();
	/*!
	* Distructer function
	*/
	//! ~LaserSensor Function
	float getRange(int);
	/*!
	* This function return float variable from ranges at index.
	*/
	//! getRange Function
	void updateSensor(float[]);
	/*!
	* This function update ranges from PioneerRobotAPI and set ranges into ranges[].
	*/
	//! updateSensor Function
	float getMin(int&);
	/*!
	* This function get minimum range from ranges[].
	*/
	//! getMin Function
	float getMax(int&);
	/*!
	* This function get maximum range from ranges[].
	*/
	//! getMax Function
	float operator [](int);
	/*!
	* This function return variable at ranges[int].
	*/
	//! operator [] Function
	float getAngle(int);
	/*!
	* This function return angle at ranges[int].
	*/
	//! getAngle Function
	float getClosestRange(float,float,float&);
	/*!
	* This function return closest range at between given ranges.
	*/
	//! ggetClosestRange Function
};

