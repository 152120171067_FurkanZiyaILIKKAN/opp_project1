#include "Path.h"

Path::Path()
{
	head = NULL;
	tail = NULL;
	number = 0;
	/*!
	\param head an Node argument, keep head of path.
	\param tail an Node argument, keep back of path.
	\param number is integer, keep how many node in path
	*/
}

Path::~Path()
{
	while (head!=NULL)
	{
		Node* silinecek;
		silinecek = head;
		head = head->next;
		delete silinecek;
	}
	tail = NULL;
}

void Path::addPose(Pose pose)
{
	if (head==NULL)
	{
		Node* node=new Node();
		node->pose = pose;
		head=node;
		tail=node;
	}
	else 
	{
		Node* node = new Node();
		node->pose = pose;
		tail->next = node;
		tail = tail->next;
	}
	number++;
}

void Path::print()
{
	Node* flag = head;
	cout << "Paths: \n";
	while (flag!=NULL)
	{
		cout << "Node's position x: | " << flag->pose.getX() << " |";
		cout << " y: | " << flag->pose.getY() << " |";
		cout << " th: | " << flag->pose.getTh() << " |";
		cout << endl;
		flag = flag->next;
	}
	/*!
	\param flag an Node argument, using forward walking in linkedlist. 
	*/
}

Node Path::operator[](int i)
{
	Node* flag = head;
	int sayac = 0;
	while (sayac!=i)
	{
		flag = flag->next;
		i++;
	}
	Node flagnode;
	flagnode.pose = flag->pose;
	flagnode.next = flag->next;
	return flagnode;
	/*!
	\param flag an Node argument, using forward walking in linkedlist.
	*/
}


Pose Path::getPose(int index)
{
	Node* flag = head;
	int sayac = -1;
	while (flag)
	{
		sayac++; 
		if (sayac==index)
		{
			break;
		}
		flag = flag->next;
		
		
	}
	if (sayac!=index)
	{
		cout << "Cant find " << index << " !!!\n Default ";
		return Pose();
	}
	else
	{
		Node flagnode;
		flagnode.pose = flag->pose;
		flagnode.next = flag->next;
		return flagnode.pose;
	}	
	
	/*!
	\param flag an Node argument, using forward walking in linkedlist.
	\param sayac is integer argument, it is just counter.
	*/
}

bool Path::removePose(int index)
{
	Node* flag = head;
	Node* trailer = new Node();
	int i = 0;
	while (flag!=NULL&&i!=index)
	{
		
		if (flag->next == NULL)
		{
			break;
		}
		i++; 
		trailer = flag;
		flag = flag->next;		
		
	}
	if (index==0)
	{
		head = head->next;
		delete trailer;
		number--;
		return true;
	}
	else if (flag==tail&&i==index)
	{
		trailer->next=flag->next;
		tail = trailer;
		number--;
		delete flag;
		return true;
	}
	else if (i==index)
	{
		trailer->next = flag->next;	
		delete flag;
		number--;
		return true;
	}
	return false;
	/*!
	\param flag an Node argument, using forward walking in linkedlist.
	\param trailer an Node argument, fallow flag from one step back.
	\param i is integer argument, it is just counter.
	*/
}

bool Path::insertPos(int index, Pose pose)
{
	bool exist = false;
	Node* flag = head;
	Node* trailer = new Node();
	int sayac = -1;
	while (flag!=NULL)
	{
		
		sayac++;
		if (sayac==index)
		{
			exist = true;
			break;
		}
		trailer = flag;
		flag = flag->next;
	}
	if (sayac==-1)
	{
		addPose(pose);
		return true;
	}
	if (exist)
	{
		if (flag->next)
		{
			trailer = flag;
			flag = flag->next;
		}
		else
		{
			addPose(pose);
			return true;
		}
		Node* node = new Node();
		node->pose = pose;
		
		node->next = flag;
		trailer->next = node;
		number++;
		return true;
	}
	else 
	{
		return false;
	}
	
	/*!
	\param flag an Node argument, using forward walking in linkedlist.
	\param trailer an Node argument, fallow flag from one step back.
	\param sayac is integer argument, it is just counter.
	\param exist is boolean argument, look is that index exist in path.
	*/
}

void Path::operator<<(int i)
{
	print();
}


Path Path::operator>>(Pose pose)
{
	addPose(pose);
	return Path();
}


