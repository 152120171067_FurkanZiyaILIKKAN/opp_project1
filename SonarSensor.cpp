#include "SonarSensor.h"


//!Constructor func connect to robot
SonarSensor::SonarSensor(PioneerRobotAPI* mainrobot)
{
    robotAPI = mainrobot;
}

//!  Distance between sensor and obstacle
//! 
float SonarSensor::getRange(int index)
{
    return ranges[index];
    cout << endl;
}
//! Max distance between sensor and obstacle
//! 
float SonarSensor::getMax(int& index)
{
    index = 0;
    float max = ranges[0];
    for (int i = 1; i <= 15; i++)
    {
        if (ranges[i] > max )
        {
            max = ranges[i]; 
            index = i;
        }
    }

 
    return max;
}
//! Min distance between sensor and obstacle
float SonarSensor::getMin(int& index)
{
    index = 0;
    float min = ranges[0];
    for (int i = 1; i <= 15; i++)
    {
        if (ranges[i]<min)
        {
            min = ranges[i];
            index = i;
        }
    }

  
    return min;
}
//! as the object moves, its distance to obstacles is also updated
void SonarSensor::updateSensor(float ranges[])
{
    robotAPI->getSonarRange(ranges);  
    for (int i = 0; i < 16; i++)
    {
        this->ranges[i] = ranges[i];
    }
}
//! It works like the getRange function
float SonarSensor::operator[](int i)
{
    return ranges[i];
}
//! Return the index of sensors angle
float SonarSensor::getAngle(int index)
{
    if (index==0|| index==15)
    {
        return 90;
    }
    else if (index ==1 || index == 14)
    {
        return 50;
    }
    else if (index == 2 || index == 13)
    {
        return 30;
    }
    else if (index == 3 || index == 12)
    {
        return 10;
    }
    else if (index == 4 || index == 11)
    {
        return -10;
    }
    else if (index == 5 || index == 10)
    {
        return -30;
    }
    else if (index == 6 || index == 9)
    {
        return -50;
    }
    else if (index == 7 || index == 8)
    {
        return -90;
    }
   
    else
    {
       cout << "Index of laser is does not exist!!" << endl;
        return -1;
    }
}

float SonarSensor::getClosestRange(float startAngle, float endAngle, float& angle)
{
    float min = ranges[(int)startAngle];
    float min_angle = 0;
    for (int a = startAngle + 1; a <= endAngle; a++)
        if (ranges[a] < min) {
            min = ranges[a];
            min_angle = a;
        }
    angle = min_angle;
    return min;
}
